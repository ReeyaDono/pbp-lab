from django import forms
from django.forms import fields, Textarea, TextInput
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['note_to', 'note_from', 'note_title', 'note_message']
        widgets = {
            'note_to': TextInput(attrs={'class':'form-control'}),
            'note_from': TextInput(attrs={'class':'form-control'}),
            'note_title': TextInput(attrs={'class':'form-control'}),
            'note_message': Textarea(attrs={'class':'form-control'}),
        }