## Perbedaan JSON dan XML
- JSON Berdasarkan notasi objek pada JavaScript, XML berupa sebuah markup language (menggunakan tag untuk merepresentasikan data).
- JSON mensupport array, XML tidak.
- JSON lebih mudah dibaca daripada XML.
- JSON hanya support UTF-8 Encoding, XML bisa bermacam-macam Encoding.
- JSON berukuran lebih kecil dan lebih cepat diproses dibanding dengan XML.

JSON dan XML sama-sama berguna untuk mengirim data. Keduanya memiliki keunggulan dan kelemahannya masing-masing dan digunakan disaat tertentu.

## Perbedaan HTML dan XML
- HTML bertujuan untuk menampilkan data. XML bertujuan untuk menyimpan dan mengirim data.
- HTML bersifat static karena bertujuan untuk menampilkan sesuatu. XML bersifat dinamis karena terjadi pertukaran data.
- Tags pada HTML terbatas. Tags pada XML bersifat extensible atau bisa bermacam-macam tergantung kebutuhan pengguna.