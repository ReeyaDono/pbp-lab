from django.http import response, HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

from .forms import FriendForm

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends' : friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = None
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3/')


    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form':form})