from django import forms
from django.forms import fields
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'DOB']
        labels = {
            'DOB': 'Birth Date',
        }
