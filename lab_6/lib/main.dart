import 'dart:js';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 6',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}


class MyHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Covid-19 Update"),
      ),
      body: ListView(
        children: [
          Image.asset(
            'images/grafik.png',
            fit: BoxFit.cover,
            ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButton("Positif"),
              _buildButton("Negatif"),
              _buildButton("Meninggal"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildNum("Jumlah Positif", "4,250,855"),
              _buildNum("Jumlah Sembuh", "4,098,178"),
              _buildNum("Jumlah Meninggal", "143,659"),
            ],
          ),
          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 10),
                color: Theme.of(context).primaryColor,
                child: const Center(
                  child: Text("Cek Data Provinsi"),
                ),
              )
            ],
          )
        ],
        )
    );
  }

  Column _buildButton(String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10),
          color: Colors.blue[600],
          width: 75,
          height: 30,
          child: Center(
              child: Text(
              label,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.white,
              ),
            ),
          )
          
        )
      ],
    );
  }

  Column _buildNum(String label, String num){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Container(
            margin: const EdgeInsets.only(top: 30),
            color: Colors.blue[600],
            width: 75,
            height: 30,
            child: Center(
                child: Text(
                label,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                ),
              ),
            ) 
          )
          ],
        ),
        Row(
          children: [
            Container(
            color: Colors.blue[600],
            width: 75,
            height: 30,
            child: Center(
                child: Text(
                num,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                ),
              ),
            ) 
          )
          ],
        ),
      ],
    );
  }

  
}