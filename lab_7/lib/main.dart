import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab_7',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const TrackerForm(),
    );
  }
}

class TrackerForm extends StatefulWidget {
  const TrackerForm({ Key? key }) : super(key: key);

  @override
  _TrackerFormState createState() => _TrackerFormState();
}

class _TrackerFormState extends State<TrackerForm> {

  final _formKey = GlobalKey<FormState>();
  final controller = TextEditingController();

  int? tipe = 1;
  int dataset = 1;

  String graph = 'graph/gr_1_1.png';

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('I-Covid Tracker'),),
      body: Column(
        children: [
          Image.asset(
            graph,
            fit: BoxFit.cover,
          ),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: controller,
                  validator: (value) {
                    if (value == null || value.isEmpty || (value != 'Harian' && value != 'Kumulatif')){
                      return 'Tolong input Harian atau Kumulatif';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: "Tuliskan Jenis dataset (Harian atau Kumulatif)"
                  ),
                ),
                RadioListTile<int>(
                  title: const Text('Positif'),
                  value: 1,
                  groupValue: this.tipe,
                  onChanged: (int? value) {
                    setState(() {
                      this.tipe = value;
                    });
                  },
                ),
                RadioListTile<int>(
                  title: const Text('Negatif'),
                  value: 2,
                  groupValue: this.tipe,
                  onChanged: (int? value) {
                    setState(() {
                      this.tipe = value;
                    });
                  },
                ),
                RadioListTile<int>(
                  title: const Text('Meninggal'),
                  value: 3,
                  groupValue: this.tipe,
                  onChanged: (int? value) {
                    setState(() {
                      this.tipe = value;
                    });
                  },
                ),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()){
                      setState(() {
                        if (controller.text == 'Harian'){
                          dataset = 2;
                        }
                        else{
                          dataset = 1;
                        }
                        graph = 'graph/gr_'+dataset.toString()+'_'+tipe.toString()+'.png';
                      });
                    }
                  }, 
                  child: const Text('Submit')
                )
              ],)
            )
        ],),
    );
  }
}